import subprocess
import sys
import tkinter as tk


def run_git(command):
    try:
        print(command)
        # 运行 git status 命令
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        # 返回命令输出
        return result.stdout
    except subprocess.CalledProcessError as e:
        # 返回命令错误输出
        return e.stderr


def show_git_status(git_command=["git", "status"]):
    # 隐藏单选按钮和确认按钮
    for rb in radio_buttons:
        rb.pack_forget()
    button.pack_forget()

    # 运行 git status 命令并显示结果
    git_data = run_git(command=git_command)
    if git_data is None:
        git_data = "空"
    result_label.config(text=git_data)
    result_label.pack(pady=20)

    # 显示返回按钮
    return_button.pack(pady=20)

def on_return_click():
    # 隐藏结果标签和返回按钮
    result_label.pack_forget()
    return_button.pack_forget()
    # 显示单选按钮和确认按钮
    for rb in radio_buttons:
        rb.pack(anchor=tk.W)
    button.pack(pady=20)


def on_button_click():
    selected_option = radio_var.get()
    label.config(text=f"当前选择: {selected_option}")
    print(f"当前选择: {selected_option}")
    if selected_option == "退出":
        sys.exit(0)
    if selected_option == "查询修改":
        show_git_status()
    # 可以在这里添加其他选项的功能
    # elif selected_option == "提交修改":
    #     pass
    # elif selected_option == "提交部分":
    #     pass


root = tk.Tk()
root.title("标签和按钮示例")
root.geometry("600x500")

label = tk.Label(root, text="进入小工具")
label.pack(pady=20)

button = tk.Button(root, text="确认", command=on_button_click)
button.pack(pady=20)

# 添加单选按钮
radio_var = tk.StringVar()
radio_var.set("查询修改")

options = ["查询修改", "提交修改", "提交部分", "退出"]
radio_buttons = []

for option in options:
    radio_button = tk.Radiobutton(root, text=option, variable=radio_var, value=option)
    radio_button.pack(anchor=tk.W)
    radio_buttons.append(radio_button)

# 添加用于显示结果的标签
result_label = tk.Label(root, text="", justify="left")

# 添加返回按钮
return_button = tk.Button(root, text="返回", command=on_return_click)


root.mainloop()
