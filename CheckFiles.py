import datetime
import subprocess
import sys

import easygui as eg


def run_git(command=['git', 'status']):
    try:
        # 运行git命令
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        # print("Git Status Output:\n", result.stdout)
        return result.stdout
    except subprocess.CalledProcessError as e:
        # 打印命令错误输出
        print("Error Output:\n", e.stderr)
        return e.stderr


# 这里解析命令并保存到列表
def explain_git(data):
    return_data = [i for i in data.split(" ")]
    return return_data


if __name__ == "__main__":
    # 修改清单
    show_data1 = run_git()
    extract_data = [i.strip("\t") for i in show_data1.split("Untracked files:")[-1].split("\n")[3:-3]]
    edit_data = [i.strip("\tmodified:").replace(" ", '') for i in
                 show_data1.split("Changes not staged for commit:")[-1].split("Untracked files:")[0].split("\n")[4:-2]]
    extract_data = extract_data + edit_data
    if ".idea/" in extract_data:
        extract_data.remove(".idea/")
    print(extract_data)
    while True:
        runCheck1 = eg.buttonbox(title="数据修改", msg="记得在浅色模式下运行",
                                 choices=("查看修改", "提交所有修改", "提交部分修改", "提取数据", "退出"))
        print(str(datetime.datetime.now()).split(".")[0] + "-->" + runCheck1)
        if runCheck1 == "查看修改":
            show_data = run_git()
            eg.codebox(text="\n".join(extract_data), title="查看修改", msg="git状态(存在修改)")
        if runCheck1 == "提交所有修改":
            show_data2 = ""
            for i in extract_data:
                show_data2 += i + "\n"
            # 全部提交
            runCheck2 = eg.ynbox(msg="确认全部提交？点击确认后请稍等", choices=["确认", "取消"])
            if runCheck2:
                for i in extract_data:
                    gitAdd = f"git add {i}"
                    run_git(command=explain_git(gitAdd))
                commitTime = datetime.datetime.now().strftime("%Y-%m-%d--%H:%M")
                gitCommit = f"git commit -m '{commitTime}'"
                run_git(command=explain_git(gitCommit))
                gitPush = "git push origin main"
                run_git(command=explain_git(gitPush))
                eg.msgbox(msg="提交成功", title="已提交", ok_button="确认")
            else:
                continue
        if runCheck1 == "提交部分修改":
            if len(extract_data) == 1:  # 如果只有一个修改
                eg.msgbox(msg="仅有一个修改值，将全部提交，点击确认后稍等", title="提交数据", ok_button="确认")
                gitAdd = f"git add {extract_data[0]}"
                run_git(command=explain_git(gitAdd))
                commitTime = datetime.datetime.now().strftime("%Y-%m-%d--%H:%M")
                gitCommit = f"git commit -m '{commitTime}'"
                run_git(command=explain_git(gitCommit))
                gitPush = "git push origin main"
                run_git(command=explain_git(gitPush))
                eg.msgbox(msg="提交成功", title="已提交", ok_button="确认")
                continue
            show_data2 = ""
            for i in extract_data:
                show_data2 += i + "\n"
            runCheck3 = eg.multchoicebox(msg=show_data2, choices=extract_data, title="选择要提交的内容")
            if runCheck3 is None:
                continue
            runCheck3_1 = eg.ynbox(msg="确认提交，点击确认后请稍等", title="提交数据", choices=["确认", '取消'])
            if runCheck3_1:
                for i in runCheck3:
                    gitAdd = f"git add {i}"
                    run_git(command=explain_git(gitAdd))
                commitTime = datetime.datetime.now().strftime("%Y-%m-%d--%H:%M")
                gitCommit = f"git commit -m '{commitTime}'"
                run_git(command=explain_git(gitCommit))
                gitPush = "git push origin main"
                run_git(command=explain_git(gitPush))
                eg.msgbox(msg="提交成功", title="已提交", ok_button="确认")
            else:
                continue
        if runCheck1 == "提取数据":
            runCheck4 = eg.buttonbox(title="数据更新", msg="你想更新数据还是拉取数据呢？",
                                     choices=("首次拉取", "数据更新", "取消"))
            if runCheck4 == "首次拉取":
                runCheck4_1 = eg.enterbox(title="输入地址", default="给我一个git地址", msg="提供一个git地址，让我来帮你拉取，提交后请稍等")
                gitClone = f"git clone {runCheck4_1}"
                cloneReturn = run_git(command=explain_git(gitClone))
                eg.codebox(text=cloneReturn, msg="拉取结果在下面:", title="拉取结果")
            if runCheck4 == "数据更新":
                gitPull = f"git pull origin main"
                run_git(command=explain_git(gitPull))
                eg.msgbox(msg="拉取成功", title="数据更新", ok_button="确认")
            if runCheck4 == "取消":
                continue
        if runCheck1 == "退出":
            sys.exit(0)
