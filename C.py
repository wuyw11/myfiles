# main.py
import datetime
import os
import subprocess
import sys
import easygui as eg

# 常量配置
GIT_REMOTE = "origin"
GIT_BRANCH = "main"


def run_git(command):
    try:
        result = subprocess.run(command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        return result.stdout
    except subprocess.CalledProcessError as e:
        return f"Command '{' '.join(e.cmd)}' returned non-zero exit status {e.returncode}.\n{e.stderr}"
    except FileNotFoundError:
        return "Git executable not found. Please ensure Git is installed and in your PATH."
    except Exception as e:
        return str(e)


def get_git_status():
    status_output = run_git(['git', 'status'])
    untracked_files = [line.strip() for line in status_output.split("Untracked files:")[-1].split("\n")[3:-3]]
    modified_files = [line.strip("modified: ").strip() for line in
                      status_output.split("Changes not staged for commit:")[-1].split("\n")[4:-2]]
    return list(set(untracked_files + modified_files))


def confirm_and_execute(commands):
    for command in commands:
        response = run_git(command)
        if "error" in response.lower():
            eg.msgbox(msg=response, title="Error", ok_button="OK")
            return False
    return True


def clone_repository(repo_url, destination_path):
    if os.path.exists(destination_path):
        choice = eg.buttonbox(msg=f"目录 '{destination_path}' 已存在。是否覆盖?", title="覆盖目录",
                              choices=["覆盖", "取消"])
        if choice == "取消":
            return "操作已取消。"
        else:
            try:
                subprocess.run(['rm', '-rf', destination_path], check=True)
            except subprocess.CalledProcessError as e:
                return f"删除目录失败：\n{e.stderr}"
    return run_git(['git', 'clone', repo_url, destination_path])


def main():
    while True:
        choice = eg.buttonbox(title="Git操作", msg="请选择一个操作：",
                              choices=["查看修改", "提交所有修改", "提交部分修改", "提取数据", "退出"])

        if choice == "查看修改":
            status_output = run_git(['git', 'status'])
            eg.codebox(msg="Git 状态输出：", title="查看修改", text=status_output)

        elif choice == "提交所有修改":
            files_to_commit = get_git_status()
            if not files_to_commit:
                eg.msgbox(msg="没有检测到修改。", title="提交所有修改", ok_button="确认")
                continue

            confirmation = eg.ynbox(msg=f"确认提交以下文件？\n{', '.join(files_to_commit)}", title="确认提交",
                                    choices=["确认", "取消"])
            if confirmation:
                commands = [["git", "add", file] for file in files_to_commit] + [
                    ["git", "commit", "-m", datetime.datetime.now().strftime("%Y-%m-%d--%H:%M")],
                    ["git", "push", GIT_REMOTE, GIT_BRANCH]]
                if confirm_and_execute(commands):
                    eg.msgbox(msg="提交成功。", title="提交所有修改", ok_button="确认")

        elif choice == "提交部分修改":
            files_to_commit = get_git_status()
            if not files_to_commit:
                eg.msgbox(msg="没有检测到修改。", title="提交部分修改", ok_button="确认")
                continue

            selected_files = eg.multchoicebox(msg="请选择要提交的文件：", title="提交部分修改", choices=files_to_commit)
            if not selected_files:
                continue

            confirmation = eg.ynbox(msg=f"确认提交以下文件？\n{', '.join(selected_files)}", title="确认提交",
                                    choices=["确认", "取消"])
            if confirmation:
                commands = [["git", "add", file] for file in selected_files] + [
                    ["git", "commit", "-m", datetime.datetime.now().strftime("%Y-%m-%d--%H:%M")],
                    ["git", "push", GIT_REMOTE, GIT_BRANCH]]
                if confirm_and_execute(commands):
                    eg.msgbox(msg="提交成功。", title="提交部分修改", ok_button="确认")

        elif choice == "提取数据":
            fetch_choice = eg.buttonbox(title="数据操作", msg="请选择一个操作：",
                                        choices=["首次拉取", "数据更新", "取消"])
            if fetch_choice == "首次拉取":
                repo_url = eg.enterbox(title="输入地址", msg="提供一个git地址：",
                                       default="https://gitlab.com/wuyw11/myfiles.git")
                if repo_url:
                    destination_path = os.path.basename(repo_url.rstrip('/').replace('.git', ''))
                    response = clone_repository(repo_url, destination_path)
                    eg.codebox(msg="拉取结果：", title="首次拉取", text=response)

            elif fetch_choice == "数据更新":
                response = run_git(["git", "pull", GIT_REMOTE, GIT_BRANCH])
                eg.msgbox(msg=response, title="数据更新", ok_button="确认")

        elif choice == "退出":
            sys.exit(0)


if __name__ == "__main__":
    main()
